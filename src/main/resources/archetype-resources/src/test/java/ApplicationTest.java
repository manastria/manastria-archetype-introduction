#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.testng.Assert;
import org.testng.annotations.*;
import fr.manastria.utils.Console;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Test
public class ApplicationTest
{
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	private String nom;

    @Test
    public void testTest() {
		testLib("ETROQUIEN Jessica");
		
		Assert.assertTrue(true);
    }
	
	private void testLib(String nom) {
		logger.debug(">>>>> testLib(nom:{})", nom);

		this.setNom(nom);
		
		Console.println("toString : {}", this.toString());
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Test(enabled = false)
	public String getNom() {
		return nom;
	}

	@Test(enabled = false)
	public void setNom(String nom) {
		this.nom = nom;
	}
}

